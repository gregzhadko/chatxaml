﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ChatApp.Model;
using ChatApp.ViewModel;

namespace ChatApp.View
{
    public class MessageDataTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var elemnt = (FrameworkElement) container;
            var message = item as TextMessage;

            if (message != null)

            {
                var parent = FindParentWindow(container);

                return
                    (DataTemplate)
                        elemnt.FindResource(((MainViewModel) parent.DataContext).OwnerContact == message.Sender
                            ? "SentMessageTemplate"
                            : "ResievedMessageTemplate");
            }

            return (DataTemplate) elemnt.FindResource("SentFileTemplate");
        }

        public static Window FindParentWindow(DependencyObject dependencyObject)
        {
            var parent = VisualTreeHelper.GetParent(dependencyObject);

            if (parent == null) return null;

            var parentT = parent as Window;
            return parentT ?? FindParentWindow(parent);
        }
    }
}
