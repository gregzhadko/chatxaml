﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace ChatApp.View
{
    public class BindableRichTextBox : RichTextBox
    {
        public static readonly DependencyProperty DocumentProperty = DependencyProperty.Register("Document",
            typeof (FlowDocument), typeof (BindableRichTextBox), new FrameworkPropertyMetadata(null, OnDocumentChanged));

        public new FlowDocument Document
        {
            get => (FlowDocument) GetValue(DocumentProperty);

            set => SetValue(DocumentProperty, value);
        }

        public static void OnDocumentChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var rtb = (RichTextBox) obj;
            rtb.Document = (FlowDocument) args.NewValue;
            double totalWidth = 13;
            foreach (var inline in ((Paragraph) rtb.Document.Blocks.FirstBlock).Inlines)
            {
                var run = inline as Run;
                if (run != null)
                {
                    totalWidth += run.Text.Length*15;
                    continue;
                }

                var link = inline as Hyperlink;
                if (link != null)
                {
                    totalWidth += link.NavigateUri.ToString().Length*15;
                }
            }

            rtb.Width = totalWidth > rtb.MaxWidth ? rtb.MaxWidth : totalWidth;
        }
    }
}
