﻿using GalaSoft.MvvmLight.Threading;

namespace ChatApp.View
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        static App()
        {
            DispatcherHelper.Initialize();
        }
    }
}