﻿using System.Collections.ObjectModel;
using System.Windows.Media;
using GalaSoft.MvvmLight;

namespace ChatApp.Model
{
    public class Chat : ObservableObject
    {
        public Chat(Contact owner, Contact sender, string title, string avatarPath)
        {
            Owner = owner;
            Sender = sender;
            Title = title;
            AvatarPath = avatarPath;
            sender.PropertyChanged += (o, args) =>
            {
                RaisePropertyChanged(nameof(Status));
                RaisePropertyChanged(nameof(StatusColor));
            };
        }

        public Chat(Contact owner, Contact sender) : this(owner, sender, sender.Name, sender.AvatarPath)
        {
        }

        public string Title { get; set; }

        public ObservableCollection<Message> Messages { get; set; } = new ObservableCollection<Message>();

        public Contact Sender { get; set; }

        public Contact Owner { get; set; }

        public string AvatarPath { get; set; }

        public SolidColorBrush StatusColor => Sender.StatusColor;

        public string Status => Sender.Status;
    }
}