﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Documents;

namespace ChatApp.Model
{
    public class TextMessage : Message
    {

        public TextMessage(Contact sender, DateTime date, string text) : base(sender, date)
        {
            Text = text;
        }

        public string Text { get; set; }

        public FlowDocument Document
        {
            get
            {
                var flowDocument = new FlowDocument();

                flowDocument.Blocks.Add(GenerateParagraph());
                
                return flowDocument;
            }
        }

        private Paragraph GenerateParagraph()
        {
            var linkParser = new Regex(@"\b(?:https?://)\S+\b", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            var startPosition = 0;

            var paragraph = new Paragraph();
            foreach (var uriString in from Match m in linkParser.Matches(Text) select m.Value)
            {
                AddTextToParagraph(
                    Text.Substring(startPosition, Text.IndexOf(uriString, StringComparison.Ordinal) - startPosition), paragraph);

                paragraph.Inlines.Add(ConstructLinkByUri(uriString));

                startPosition = Text.LastIndexOf(uriString, StringComparison.Ordinal) + uriString.Length;
            }

            var endText = Text.Substring(startPosition, Text.Length - startPosition);
            AddTextToParagraph(endText, paragraph);
            return paragraph;
        }

        private static void AddTextToParagraph(string text, Paragraph paragraph)
        {
            if (!String.IsNullOrEmpty(text))
            {
                paragraph.Inlines.Add(text);
            }
        }

        private static Hyperlink ConstructLinkByUri(string uriString)
        {
            var link = new Hyperlink {NavigateUri = new Uri(uriString)};
            link.Inlines.Add(uriString);
            link.RequestNavigate += (sender, args) => { Process.Start(uriString); };
            return link;
        }


    }
}