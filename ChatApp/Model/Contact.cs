﻿using System.Windows.Media;
using GalaSoft.MvvmLight;

namespace ChatApp.Model
{
    public class Contact : ObservableObject
    {
        private bool _isOnline;

        public Contact(string name) : this(name, "nopic.png")
        {
        }

        public Contact(string name, string avatarName)
        {
            Name = name;

            AvatarPath = @"..\Resources\" + avatarName;
        }

        public string Name { get; set; }

        public string AvatarPath { get; set; }

        public bool IsOwner { get; set; }

        public bool IsOnline
        {
            get => _isOnline;
            set { Set(() => IsOnline, ref _isOnline, value); }
        }

        public SolidColorBrush StatusColor
        {
            get
            {
                var color = ColorConverter.ConvertFromString(IsOnline ? "#FF89C076" : "#FAE90000");
                return color == null ? null : new SolidColorBrush((Color)color);
            }
        }

        public string Status => IsOnline ? "Online" : "Offline";
    }
}