﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace ChatApp.Model
{
    public class MessegeService
    {
        public void SendMessage(Message message)
        {
            SendMessageAsync(message);
        }

        private void SendMessageAsync(Message textMessage)
        {
            Debug.Print("Sending...");
            var t = new Task(() =>
            {
                Thread.Sleep(5000);
            });
            t.Start();
        }

        public void RecieveMessage(Chat chat)
        {
            RecieveMessageAsync(chat);
        }

        private void RecieveMessageAsync(Chat chat)
        {
            Debug.Print("Recieving...");
            var t = new Task(() =>
            {
                while (true)
                {
                    Thread.Sleep(5000);
                    Application.Current?.Dispatcher.Invoke(
                        () => chat?.Messages?.Add(new TextMessage(chat.Sender, DateTime.Now, "RandomText")));
                }
            });

            t.Start();
        }
    }
}