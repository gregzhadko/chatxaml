﻿using System;
using System.Windows.Media;

namespace ChatApp.Model
{
    public class Message
    {
        public Message() : this(null, DateTime.Now) { }


        public Message(Contact sender, DateTime date)
        {
            Date = date;
            Sender = sender;
        }

        public DateTime Date { get; set; }
        public Contact Sender { get; set; }

        public SolidColorBrush BackgroundColor
        {
            get
            {
                var color = ColorConverter.ConvertFromString(Sender.IsOwner ? "#EEFCDD" : "#FFFFFF");
                return color == null ? null : new SolidColorBrush((Color) color);
            }
        }
    }
}