﻿using System;
using System.IO;

namespace ChatApp.Model
{
    public class FileMessage : Message
    {
        public FileMessage(Contact sender, DateTime date, FileInfo fileInfo) : base(sender, date)
        {
            FileInfo = fileInfo;
        }

        public FileInfo FileInfo { get; set; }
    }
}
