﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Input;
using ChatApp.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Win32;

namespace ChatApp.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private readonly MessegeService _messegeService = new MessegeService();
        private ObservableCollection<Chat> _chats = new ObservableCollection<Chat>();
        private string _enteredText;
        private Contact _ownerContact;
        private Chat _selectedChat;


        public MainViewModel()
        {
            OwnerContact = new Contact("Owner", "cat.png") {IsOwner = true};
            var contact1 = new Contact("Doggy", "dog.png") {IsOnline = true};
            var contact2 = new Contact("Pig", "pig.png");
            var contact3 = new Contact("Another Pig", "pig.png");
            var contact4 = new Contact("No name") { IsOnline = true };

            var chat1 = new Chat(OwnerContact, contact1);
            chat1.Messages.Add(new TextMessage(contact1, DateTime.Now.AddMinutes(-5), "Hello"));
            chat1.Messages.Add(new TextMessage(OwnerContact, DateTime.Now.AddMinutes(-4), "Check the link http://google.com"));
            chat1.Messages.Add(new TextMessage(contact1, DateTime.Now.AddMinutes(-3), "How Are you?"));
            chat1.Messages.Add(new TextMessage(OwnerContact, DateTime.Now.AddMinutes(-2), "Fine"));

            var chat2 = new Chat(OwnerContact, contact2);
            chat2.Messages.Add(new TextMessage(contact1, DateTime.Now.AddMinutes(-5), "Hello"));
            chat2.Messages.Add(new TextMessage(OwnerContact, DateTime.Now.AddMinutes(-4), "Hi"));
            chat2.Messages.Add(new TextMessage(contact1, DateTime.Now.AddMinutes(-3), "Bye"));
            chat2.Messages.Add(new TextMessage(OwnerContact, DateTime.Now.AddMinutes(-2), "See you later"));

            var chat3 = new Chat(OwnerContact, contact3);
            chat3.Messages.Add(new TextMessage(contact1, DateTime.Now.AddMinutes(-5), "Are you here?"));
            chat3.Messages.Add(new TextMessage(OwnerContact, DateTime.Now.AddMinutes(-4), "Yes"));

            var chat4 = new Chat(OwnerContact, contact4);
            chat4.Messages.Add(new TextMessage(contact1, DateTime.Now.AddMinutes(-5), "Hello"));
            chat4.Messages.Add(new TextMessage(OwnerContact, DateTime.Now.AddMinutes(-4), "Hi"));
            chat4.Messages.Add(new TextMessage(contact1, DateTime.Now.AddMinutes(-3), "How Are you?"));
            chat4.Messages.Add(new TextMessage(OwnerContact, DateTime.Now.AddMinutes(-2), "Fine"));


            Chats.Add(chat1);
            Chats.Add(chat2);
            Chats.Add(chat3);
            Chats.Add(chat4);

            SelectedChat = chat1;
            //_messegeService.RecieveMessage(SelectedChat);
            SelectedChat.PropertyChanged += (sender, args) =>
            {
                RaisePropertyChanged(nameof(Chats));
            };

        }

        public Contact OwnerContact
        {
            get => _ownerContact;
            set => Set(ref _ownerContact, value);
        }

        public Chat SelectedChat
        {
            get => _selectedChat;
            set => Set(ref _selectedChat, value);
        }

        public ObservableCollection<Chat> Chats
        {
            get => _chats;
            set => Set(ref _chats, value);
        }

        public string EnteredText
        {
            get => _enteredText;
            set => Set(ref _enteredText, value);
        }

        public ICommand SendCommand => new RelayCommand(SendMessage);

        public RelayCommand OpenFileCommand
        {
            get { return new RelayCommand(() =>
            {
                var dialog = new OpenFileDialog();
                if (dialog.ShowDialog() == true)
                {
                    FileInfo info = new FileInfo(dialog.FileName);
                    var message = new FileMessage(OwnerContact, DateTime.Now, info);
                    _messegeService.SendMessage(message);
                    SelectedChat.Messages.Add(message);
                }
                
            });}
        }

        private void SendMessage()
        {
            if (String.IsNullOrWhiteSpace(EnteredText))
            {
                return;
            }

            var message = new TextMessage(OwnerContact, DateTime.Now, EnteredText);

            _messegeService.SendMessage(message);


            SelectedChat.Messages.Add(message);

            EnteredText = String.Empty;
        }
    }
}